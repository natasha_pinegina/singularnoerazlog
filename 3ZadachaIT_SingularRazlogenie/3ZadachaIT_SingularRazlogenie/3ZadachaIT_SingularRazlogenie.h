﻿
// 3ZadachaIT_SingularRazlogenie.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CMy3ZadachaITSingularRazlogenieApp:
// Сведения о реализации этого класса: 3ZadachaIT_SingularRazlogenie.cpp
//

class CMy3ZadachaITSingularRazlogenieApp : public CWinApp
{
public:
	CMy3ZadachaITSingularRazlogenieApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CMy3ZadachaITSingularRazlogenieApp theApp;
