﻿
// 3ZadachaIT_SingularRazlogenieDlg.cpp: файл реализации
//
#define _USE_MATH_DEFINES
#include <iostream>
#include "pch.h"
#include "framework.h"
#include "3ZadachaIT_SingularRazlogenie.h"
#include "3ZadachaIT_SingularRazlogenieDlg.h"
#include "afxdialogex.h"
#include "math.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

namespace mylib
{
	std::string to_string(const double& d)
	{
		std::ostringstream strm;
		strm << d;
		return strm.str();
	}
}

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define KOORD(x,y) (xp*((x)-xmin)),(yp*((y)-ymax)) 
#define KOORDGET(x,y) (xpget*((x)-xminget)),(ypget*((y)-ymaxget)) 
#define KOORDSOB(x,y) (xps*((x)-xmins)),(yps*((y)-ymaxs)) 

// Диалоговое окно CMy3ZadachaITSingularRazlogenieDlg



CMy3ZadachaITSingularRazlogenieDlg::CMy3ZadachaITSingularRazlogenieDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MY3ZADACHAIT_SINGULARRAZLOGENIE_DIALOG, pParent)
	, N(3)
	, determinant(0)
	, M(3)
	, A1(3)
	, A2(1)
	, A3(4)
	, t01(120)
	, t02(350)
	, t03(500)
	, G1(20)
	, G2(10)
	, G3(20)
	, Time(600)
	, F1(0.005)
	, F2(0.006)
	, F3(0.007)
	, P1(0)
	, P2(0)
	, P3(0)
	, P(50)
	, nev(0)
	, Info(_T(""))
	, number(1)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy3ZadachaITSingularRazlogenieDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_N, N);
	DDX_Text(pDX, IDC_Determinant, determinant);
	DDX_Text(pDX, IDC_N2, M);
	DDX_Text(pDX, IDC_A1, A1);
	DDX_Text(pDX, IDC_A2, A2);
	DDX_Text(pDX, IDC_A3, A3);
	DDX_Text(pDX, IDC_t01, t01);
	DDX_Text(pDX, IDC_t02, t02);
	DDX_Text(pDX, IDC_t03, t03);
	DDX_Text(pDX, IDC_G1, G1);
	DDX_Text(pDX, IDC_G2, G2);
	DDX_Text(pDX, IDC_G3, G3);
	DDX_Text(pDX, IDC_Time, Time);
	DDX_Control(pDX, IDC_Garmon, Garmon);
	DDX_Control(pDX, IDC_Gaus, Gaus);
	DDX_Control(pDX, IDC_Zatux, Zatux);
	DDX_Text(pDX, IDC_F1, F1);
	DDX_Text(pDX, IDC_F2, F2);
	DDX_Text(pDX, IDC_F3, F3);
	DDX_Text(pDX, IDC_P1, P1);
	DDX_Text(pDX, IDC_P2, P2);
	DDX_Text(pDX, IDC_P3, P3);
	DDX_Text(pDX, IDC_P, P);
	DDX_Text(pDX, IDC_Nev, nev);
	DDX_Text(pDX, IDC_Info, Info);
	DDX_Text(pDX, IDC_P4, number);
}

BEGIN_MESSAGE_MAP(CMy3ZadachaITSingularRazlogenieDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SingRazlog, &CMy3ZadachaITSingularRazlogenieDlg::OnBnClickedSingrazlog)
	ON_BN_CLICKED(IDC_GetSignal, &CMy3ZadachaITSingularRazlogenieDlg::OnBnClickedGetsignal)
	
END_MESSAGE_MAP()


// Обработчики сообщений CMy3ZadachaITSingularRazlogenieDlg

BOOL CMy3ZadachaITSingularRazlogenieDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	PicWndInitalData = GetDlgItem(IDC_InputSignal);
	PicDcInitalData = PicWndInitalData->GetDC();
	PicWndInitalData->GetClientRect(&PicInitalData);

	PicWndGetData = GetDlgItem(IDC_SobctChisla);
	PicDcGetData = PicWndGetData->GetDC();
	PicWndGetData->GetClientRect(&PicGetData);

	PicWndSobstv = GetDlgItem(IDC_SobctvVect);
	PicDcSobstv = PicWndSobstv->GetDC();
	PicWndSobstv->GetClientRect(&PicSobstv);

	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		0.1,						//толщина 1 пиксель
		RGB(0, 0, 250));		//цвет  черный
	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(0, 0, 0));			//цвет черный

	graf_pen.CreatePen(			//график
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет черный
	graf_pen2.CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	graf_pen3.CreatePen(PS_SOLID, 2, RGB(150, 150, 0));
	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_Garmon));
	pcb1->SetCheck(1);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CMy3ZadachaITSingularRazlogenieDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CMy3ZadachaITSingularRazlogenieDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
double CMy3ZadachaITSingularRazlogenieDlg::InputSignal(int t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Sigma[] = { G1, G2, G3 };
	double Centers_of_Gaussian_domes[] = { t01, t02, t03 };
	double result = 0;
	for (int i = 0; i <= 2; i++)
	{
		result += Amplitude[i] * exp(-((t - Centers_of_Gaussian_domes[i]) / Sigma[i]) * ((t - Centers_of_Gaussian_domes[i]) / Sigma[i]));
	}
	return result;
}
double CMy3ZadachaITSingularRazlogenieDlg::PolySignal(double t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Frequency[] = { F1, F2, F3 };
	double Phase[] = { P1, P2, P3 };
	double result = 0;
	for (int i = 0; i <= 2; i++)
	{
		result += Amplitude[i] * sin(2 * 3.14159 * Frequency[i] * t + Phase[i]);
	}
	return result;
}
double CMy3ZadachaITSingularRazlogenieDlg::ZatuxSignal(double t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Frequency[] = { F1, F2, F3 };
	double Phase[] = { P1, P2, P3 };
	double result = 0;
	for (int i = 0; i <= 2; i++)
	{
		result += exp(-Amplitude[i]*t)* sin(2 * 3.14159 * Frequency[i] * t + Phase[i]);
	}
	return result;
}

void CMy3ZadachaITSingularRazlogenieDlg::PererisovkaInitalData()
{

	xmin = -2;			//минимальное значение х
	xmax = Time;			//максимальное значение х
	if (Gaus.GetCheck())
		ymin = -MaxInitalData / 10 - 0.5;		//минимальное значение y
	else
		ymin = -MaxInitalData - 4;
	ymax = MaxInitalData + 4;//максимальное значение y


	xp = ((double)(PicInitalData.Width()) / (xmax - xmin));			//Коэффициенты пересчёта координат по Х
	yp = -((double)(PicInitalData.Height()) / (ymax - ymin));

	PicDcInitalData->FillSolidRect(&PicInitalData, RGB(250, 250, 250));			//закрашиваю фон 


	PicDcInitalData->SelectObject(&osi_pen);
	//создаём Ось Y
	PicDcInitalData->MoveTo(KOORD(1, ymax));
	PicDcInitalData->LineTo(KOORD(1, ymin));
	//создаём Ось Х
	PicDcInitalData->MoveTo(KOORD(xmin, 0));
	PicDcInitalData->LineTo(KOORD(xmax, 0));

	//подпись осей
	PicDcInitalData->TextOutW(KOORD(0, ymax - 0.2), _T("Y")); //Y
	PicDcInitalData->TextOutW(KOORD(xmax - 0.3, 0 - 0.2), _T("t")); //X

	PicDcInitalData->SelectObject(&setka_pen);

	//отрисовка сетки по y
	for (float x = xmin + 1; x <= xmax; x += xmax / 10)
	{
		PicDcInitalData->MoveTo(KOORD(x, ymax));
		PicDcInitalData->LineTo(KOORD(x, ymin));
	}
	//отрисовка сетки по x
	for (float y = ymin + 1; y <= ymax; y += ymax / 5)
	{
		PicDcInitalData->MoveTo(KOORD(xmin, y));
		PicDcInitalData->LineTo(KOORD(xmax, y));
	}


	//подпись точек на оси
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcInitalData->SelectObject(font3);
	//по Y с шагом 5
	for (double i = ymin; i <= ymax; i += ymax / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDcInitalData->TextOutW(KOORD(0.2, i), str);
	}
	//по X с шагом 0.5
	for (double j = xmin; j <= xmax; j += xmax / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDcInitalData->TextOutW(KOORD(j - 0.25, -0.2), str);
	}
}

void CMy3ZadachaITSingularRazlogenieDlg::PererisovkaVector()
{

	xmins = -2;			//минимальное значение х
	xmaxs = P;			//максимальное значение х
	ymaxs = VectorMax;
	ymins = -VectorMax;

	xps = ((double)(PicSobstv.Width()) / (xmaxs - xmins));			//Коэффициенты пересчёта координат по Х
	yps = -((double)(PicSobstv.Height()) / (ymaxs - ymins));

	PicDcSobstv->FillSolidRect(&PicSobstv, RGB(250, 250, 250));			//закрашиваю фон 


	PicDcSobstv->SelectObject(&osi_pen);
	//создаём Ось Y
	PicDcSobstv->MoveTo(KOORDSOB(0, ymaxs));
	PicDcSobstv->LineTo(KOORDSOB(0, ymins));
	//создаём Ось Х
	PicDcSobstv->MoveTo(KOORDSOB(xmins, 0));
	PicDcSobstv->LineTo(KOORDSOB(xmaxs, 0));

	//подпись осей
	PicDcSobstv->TextOutW(KOORDSOB(0, ymaxs - 0.2), _T("Y")); //Y
	PicDcSobstv->TextOutW(KOORDSOB(xmaxs - 0.3, 0 - 0.2), _T("t")); //X

	PicDcSobstv->SelectObject(&setka_pen);

	//отрисовка сетки по y
	for (float x = xmins + 1; x <= xmaxs; x += xmaxs / 10)
	{
		PicDcSobstv->MoveTo(KOORDSOB(x, ymaxs));
		PicDcSobstv->LineTo(KOORDSOB(x, ymins));
	}
	//отрисовка сетки по x
	for (float y = ymins + 1; y <= ymaxs; y += ymaxs / 5)
	{
		PicDcSobstv->MoveTo(KOORDSOB(xmins, y));
		PicDcSobstv->LineTo(KOORDSOB(xmaxs, y));
	}


	//подпись точек на оси
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcSobstv->SelectObject(font3);
	//по Y с шагом 5
	for (double i = ymins; i <= ymaxs; i += ymaxs / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDcSobstv->TextOutW(KOORDSOB(0.2, i), str);
	}
	//по X с шагом 0.5
	for (double j = xmins; j <= xmaxs; j += xmaxs / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDcSobstv->TextOutW(KOORDSOB(j - 0.25, -0.2), str);
	}
}

// Функция для создания матрицы; n - строка, m -  стлбец
double** matrix(int n, int m)
{
	double** matr = new double* [n];
	for (int i = 0; i < n; i++)
		matr[i] = new double[m];
	return matr;
}

// Единичная матрица
double** Unit_Matrix(int n)
{                                                     //функция создает единичную матрицу (квадратную)
	double** E = matrix(n, n);                        //она состоит из 0 и 1
	for (int i = 0; i < n; i++)                       //ее главная диоганаль заполнена 1, все остальные элементы равны 0
		for (int j = 0; j < n; j++)
			E[i][j] = ((i == j) ? 1 : 0);
	return E;
}

// Исходная матрица 
double** Inital_Matrix(int n,int m)
{
	srand(time(0));                                                //функция используется для создания исходной матрицы 
	double** Inital = matrix(n, m);                        //исходная матрица является квадратной 
	int kol = 0;
	for (int i = 0; i < n; i++)                       //элементы получены с помощью функции function
		for (int j = 0; j < m; j++)
			//Inital[i][j] = 2 * (rand() % 100) / (100 * 1.0) - 1;
		{
			Inital[i][j] = kol + 1.01; kol++;
		}
	return Inital;
}
double* Stolbec(int m)
{
	srand(time(0));                                                //функция используется для создания исходной матрицы 
	double* Inital =new double[m];                        //исходная матрица является квадратной 
	int kol = 0;                  
		for (int j = 0; j < m; j++)
			Inital[j] = 2 * (rand() % 100) / (100 * 1.0) - 1;
	return Inital;
}

// Формирование расширенной матрицы 
double** Extended_Matrix(int n, int m, double** Inital, double** Unit)
{                                                                            //функция создает расширенную матрицу размером n*2n
	double** Extended = matrix(n, m);                                        //она состоит из исходной матрицы и единичной матрицы
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			Extended[i][j] = ((j < n) ? Inital[i][j] : Unit[i][j - n]);
	}
	return Extended;
}

// Прямой ход метода Гаусса
double** Direct_Method_Gauss(double** Extended_Matrix, int n, int t)
{
	double temp(0);

	for (int i = 0; i < n; i++)
		for (int j = i + 1; j < n; j++) {
			temp = (Extended_Matrix[i][j] / Extended_Matrix[i][i]);
			for (int k = i; k < t; k++)
				Extended_Matrix[j][k] -= (Extended_Matrix[i][k] * temp);
		}
	return Extended_Matrix;
}
// Нахождение детерминанта 
// Произведение диагональных элементов треугольной матрицы после прямого хода равно детерминанту исходной матрицы
double Determinant(double** Extended_Matrix, int n)
{
	double determinant = 1;
	for (int i = 0; i < n; i++)
		determinant *= Extended_Matrix[i][i];
	return determinant;
}

// Обратный ход метода Гаусса
//Строчка "с" - коэффициенты полинома(элементы столбцов обратной матрицы)
//Зануляем "с" и начиная с последнего находим c[i].
double* Reverse_Method_Gauss(double** Extended_Matrix, int n, int s)
{
	double* c = new double[n];
	for (int i = 0; i < n; i++)
		c[i] = 0;
	double tempsum(0);
	for (int l = (n - 1); l >= 0; l--)
	{
		for (int k = 0; k < n; k++)
			tempsum += Extended_Matrix[l][k] * c[k];
		c[l] = (Extended_Matrix[l][s] - tempsum) / Extended_Matrix[l][l];
		tempsum = 0;

	}
	return c;
}

// Метод формирующий обратную матрицу
//*c = 0 - это "переменная" строчки; указатель на нулевой элемент какой-либо строчки. 
//Получаем в цикле строчку, используя обратный ход, передаём ее в "переменную" и работаем с элементами
double** Inverse_Matrix(double** Extended_Matrix, int n)
{
	double** Inverse = matrix(n, n), * c = 0;

	for (int s = 0; s < n; s++)
	{
		c = Reverse_Method_Gauss(Extended_Matrix, n, (s + n));
		for (int i = 0; i < n; i++)
		{
			Inverse[i][s] = c[i];
		}
	}
	return Inverse;
}

// Произведение матриц
double* Composition_Matrix_Stroka(double** Inital_Matrix, double* Inverse_Matrix, int n, int m)
{
	double* Composition = new double[m];
	for (int i = 0; i < m; i++)
			Composition[i] = 0;

	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
				Composition[i] += Inital_Matrix[i][j] * Inverse_Matrix[j];
	return Composition;
}

double** transpose(double** matr,int m,int n) 
{
	double** res = matrix(n, m);

	for (int i = 0; i < m; ++i)
		for (int j = 0; j < n; ++j)
			res[j][i] = matr[i][j];

	return res;
}

int svd_hestenes(int m_m, int n_n, double* a, double* u, double* v, double* sigma)
{
	double thr = 1.E-4f, nul = 1.E-16f;
	int n, m, i, j, l, k, lort, iter, in, ll, kk;
	double alfa, betta, hamma, eta, t, cos0, sin0, buf, s;
	n = n_n;
	m = m_m;
	for (i = 0; i < n; i++)
	{
		in = i * n;
		for (j = 0; j < n; j++)
			if (i == j) v[in + j] = 1.;
			else v[in + j] = 0.;
	}
	for (i = 0; i < m; i++)
	{
		in = i * n;
		for (j = 0; j < n; j++)
		{
			u[in + j] = a[in + j];
		}
	}

	iter = 0;
	while (1)
	{
		lort = 0;
		iter++;
		for (l = 0; l < n - 1; l++)
			for (k = l + 1; k < n; k++)
			{
				alfa = 0.; betta = 0.; hamma = 0.;
				for (i = 0; i < m; i++)
				{
					in = i * n;
					ll = in + l;
					kk = in + k;
					alfa += u[ll] * u[ll];
					betta += u[kk] * u[kk];
					hamma += u[ll] * u[kk];
				}

				if (sqrt(alfa * betta) < nul)	continue;
				if (fabs(hamma) / sqrt(alfa * betta) < thr) continue;

				lort = 1;
				eta = (betta - alfa) / (2.f * hamma);
				t = (double)((eta / fabs(eta)) / (fabs(eta) + sqrt(1. + eta * eta)));
				cos0 = (double)(1. / sqrt(1. + t * t));
				sin0 = t * cos0;

				for (i = 0; i < m; i++)
				{
					in = i * n;
					buf = u[in + l] * cos0 - u[in + k] * sin0;
					u[in + k] = u[in + l] * sin0 + u[in + k] * cos0;
					u[in + l] = buf;

					if (i >= n) continue;
					buf = v[in + l] * cos0 - v[in + k] * sin0;
					v[in + k] = v[in + l] * sin0 + v[in + k] * cos0;
					v[in + l] = buf;
				}
			}

		if (!lort) break;
	}

	for (i = 0; i < n; i++)
	{
		s = 0.;
		for (j = 0; j < m; j++)	s += u[j * n + i] * u[j * n + i];
		s = (double)sqrt(s);
		sigma[i] = s;
		if (s < nul)	continue;
		for (j = 0; j < m; j++)	u[j * n + i] /= s;
	}
	//======= Sortirovka ==============
	for (i = 0; i < n - 1; i++)
		for (j = i; j < n; j++)
			if (sigma[i] < sigma[j])
			{
				s = sigma[i]; sigma[i] = sigma[j]; sigma[j] = s;
				for (k = 0; k < m; k++)
				{
					s = u[i + k * n]; u[i + k * n] = u[j + k * n]; u[j + k * n] = s;
				}
				for (k = 0; k < n; k++)
				{
					s = v[i + k * n]; v[i + k * n] = v[j + k * n]; v[j + k * n] = s;
				}
			}

	return iter;
}

double** Composition_Matrix_Two(double** Inital_Matrix, double** Inverse_Matrix, int n)
{
	double** Composition = matrix(n, n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			Composition[i][j] = 0;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			for (int k = 0; k < n; k++)
				Composition[i][j] += Inital_Matrix[i][k] * Inverse_Matrix[k][j];
	return Composition;

}

double** Composition_Matrix_Raz(double** matrix1, double** matrix2, int n,int m,int k)
{
	double** Composition = matrix(n, k);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < k; j++)
			Composition[i][j] = 0;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < k; j++)
			for (int l= 0; l < m; l++)
				Composition[i][j] += matrix1[i][l] * matrix2[l][j];
	return Composition;
}
void CMy3ZadachaITSingularRazlogenieDlg::PererisovkaGetData()
{

	xminget = -2;			//минимальное значение х
	xmaxget = P;			//максимальное значение х
	yminget = -MaxGetData / 10 - 0.5;		//минимальное значение y
	ymaxget = MaxGetData;//максимальное значение y


	xpget = ((double)(PicGetData.Width()) / (xmaxget - xminget));			//Коэффициенты пересчёта координат по Х
	ypget = -((double)(PicGetData.Height()) / (ymaxget - yminget));

	PicDcGetData->FillSolidRect(&PicGetData, RGB(250, 250, 250));			//закрашиваю фон 


	PicDcGetData->SelectObject(&osi_pen);
	//создаём Ось Y
	PicDcGetData->MoveTo(KOORDGET(0, ymaxget));
	PicDcGetData->LineTo(KOORDGET(0, yminget));
	//создаём Ось Х
	PicDcGetData->MoveTo(KOORDGET(xminget, 0));
	PicDcGetData->LineTo(KOORDGET(xmaxget, 0));

	//подпись осей
	PicDcGetData->TextOutW(KOORDGET(0, ymaxget - 0.2), _T("Y")); //Y
	PicDcGetData->TextOutW(KOORDGET(xmaxget - 0.3, 0 - 0.2), _T("t")); //X

	PicDcGetData->SelectObject(&setka_pen);

	//отрисовка сетки по y
	for (float x = xminget + 1; x <= xmaxget; x += xmaxget / 10)
	{
		PicDcGetData->MoveTo(KOORDGET(x, ymaxget));
		PicDcGetData->LineTo(KOORDGET(x, yminget));
	}
	//отрисовка сетки по x
	for (float y = yminget + 1; y <= ymaxget; y += ymaxget / 5)
	{
		PicDcGetData->MoveTo(KOORDGET(xminget, y));
		PicDcGetData->LineTo(KOORDGET(xmaxget, y));
	}


	//подпись точек на оси
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcGetData->SelectObject(font3);
	//по Y с шагом 5
	for (double i = yminget; i <= ymaxget; i += ymaxget / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDcGetData->TextOutW(KOORDGET(0.2, i), str);
	}
	//по X с шагом 0.5
	for (double j = xminget; j <= xmaxget; j += xmaxget / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDcGetData->TextOutW(KOORDGET(j - 0.25, -0.2), str);
	}
}

void CMy3ZadachaITSingularRazlogenieDlg::OnBnClickedSingrazlog()
{
	
	UpdateData(true);
	string res = "";
	Info = res.c_str();
	double** Inital = Inital_Matrix(N,M);
	FILE* results1;
	Info.Format(_T("Исходный массив:\r\n"));
	if (fopen_s(&results1, "Исходная матрица.txt", "w") == 0)
	{
		for (int i = 0; i < N; i++)
		{
			res = "";
			fprintf(results1, "| ");
			for(int j=0;j<M;j++)
			{ 
			fprintf(results1, "%+*.3f ", 10, Inital[i][j]);
			res =res+ mylib::to_string(Inital[i][j])+"  ";
			if (j == M - 1)
				res += "\r\n";
			}
			fprintf(results1, "|\n");
			Info+=res.c_str();
		}
	}
	if (N < M)
	{
		double** TransA = transpose(Inital,N,M);
		double** FirstUmn = Composition_Matrix_Raz(Inital,TransA,N,M,N);
		FILE* results15;
		res = "";
		res =res+ "\r\n"+"Умножение исходной матрицы на транспонированную"+"\r\n";
		Info += res.c_str();
		if (fopen_s(&results15, "Умножение на транспонированную.txt", "w") == 0)
		{
			for (int i = 0; i < N; i++)
			{
				res = "";
				fprintf(results15, "| ");
				for (int j = 0; j < N; j++)
				{
					fprintf(results15, "%+*.3f ", 10, FirstUmn[i][j]);
					res = res + mylib::to_string(FirstUmn[i][j]) + "  ";
					if (j == N - 1)
						res += "\r\n";
				}
				fprintf(results15, "|\n");
				Info += res.c_str();
			}
		}
		double** Inverse = Inverse_Matrix(Direct_Method_Gauss(Extended_Matrix(N, 2 * N, FirstUmn, Unit_Matrix(N)), N, 2 * N), N);
		FILE* results16;
		res = "";
		res = res + "\r\n" + "Обратная матрица" + "\r\n";
		Info += res.c_str();
		if (fopen_s(&results16, "Обратная.txt", "w") == 0)
		{
			res = "";
			for (int i = 0; i < N; i++)
			{
				fprintf(results16, "| ");
				for (int j = 0; j < N; j++)
				{
					fprintf(results16, "%+*.3f ", 10, Inverse[i][j]);
					res = res + mylib::to_string(Inverse[i][j]) + "  ";
					if (j == N - 1)
						res += "\r\n";
				}
				fprintf(results16, "|\n");
				Info += res.c_str();
			}
		}
		double** PsevdoA= Composition_Matrix_Raz(TransA, Inverse, M, N, N);
		FILE* results17;
		res = "";
		res = res + "\r\n" + "Псевдообратная матрица" + "\r\n";
		Info += res.c_str();
		if (fopen_s(&results17, "ПсевдоА.txt", "w") == 0)
		{
			for (int i = 0; i < M; i++)
			{
				res = "";
				fprintf(results17, "| ");
				for (int j = 0; j < N; j++)
				{
					fprintf(results17, "%+*.5f ", 10, PsevdoA[i][j]);
					res = res + mylib::to_string(PsevdoA[i][j]) + "  ";
					if (j == N - 1)
						res += "\r\n";
				}
				fprintf(results17, "|\n");
				Info += res.c_str();
			}
		}
		double* stolbec = Stolbec(N);
		FILE* results18;
		res = "";
		res = res + "\r\n" + "Столбец свободных членов" + "\r\n";
		//Info += res.c_str();
		if (fopen_s(&results18, "Столбец свободных членов.txt", "w") == 0)
		{
			for (int i = 0; i < N; i++)
			{
					fprintf(results18, "%+*.3f ", 10, stolbec[i]);
					res = res + mylib::to_string(stolbec[i]) + "  ";
			}
			Info += res.c_str();
		}

		double* x = Composition_Matrix_Stroka(PsevdoA, stolbec, N, M);
		FILE* results19;
		res = "";
		res = res + "\r\n" + "x:" + "\r\n";
		if (fopen_s(&results19, "x.txt", "w") == 0)
		{
			for (int i = 0; i < M; i++)
			{
				fprintf(results19, "%+*.3f ", 10, x[i]);
				res = res + mylib::to_string(x[i]) + "  ";
			}
			Info += res.c_str();
		}
		
		double* b= Composition_Matrix_Stroka(Inital, x, M,N);
		FILE* results20;
		res = "";
		res = res + "\r\n" + "b:" + "\r\n";
		if (fopen_s(&results20, "b.txt", "w") == 0)
		{
			for (int i = 0; i < N; i++)
			{
				fprintf(results20, "%+*.3f ", 10, b[i]);
				res = res + mylib::to_string(b[i]) + "  ";
			}
			Info += res.c_str();
		}
		double sum = 0;
		for (int i = 0; i < N; i++)
		{
			sum += (stolbec[i] - b[i]) * (stolbec[i] - b[i]);
		}
		nev = sum / M;
	}
	else
	{ 
		double** TransA = transpose(Inital, N, M);
		determinant = Determinant(Direct_Method_Gauss(Extended_Matrix(N, 2 * M, Inital, Unit_Matrix(M)), N, 2 * M),N);
		double** FirstUmn = Composition_Matrix_Raz(TransA, Inital, M, N, M);
		FILE* results15;
		res = "";
		res = res + "\r\n" + "Умножение транспонированной на исходную:" + "\r\n";
		Info += res.c_str();
		if (fopen_s(&results15, "Умножение на транспонированную.txt", "w") == 0)
		{
			for (int i = 0; i < M; i++)
			{
				res = "";
				fprintf(results15, "| ");
				for (int j = 0; j < M; j++)
				{
					fprintf(results15, "%+*.3f ", 10, FirstUmn[i][j]);
					res = res + mylib::to_string(FirstUmn[i][j]) + "  ";
					if (j == N - 1)
						res += "\r\n";
				}
				fprintf(results15, "|\n");
				Info += res.c_str();
			}
		}
		double** Inverse = Inverse_Matrix(Direct_Method_Gauss(Extended_Matrix(N, 2 * M, FirstUmn, Unit_Matrix(M)), N, 2 * M), M);
		FILE* results16;
		res = "";
		res = res + "\r\n" + "Обратная матрица:" + "\r\n";
		Info += res.c_str();
		if (fopen_s(&results16, "Обратная.txt", "w") == 0)
		{
			for (int i = 0; i < N; i++)
			{
				res = "";
				fprintf(results16, "| ");
				for (int j = 0; j < N; j++)
				{
					fprintf(results16, "%+*.3f ", 10, Inverse[i][j]);
					res = res + mylib::to_string(Inverse[i][j]) + "  ";
					if (j == N - 1)
						res += "\r\n";
				}
				fprintf(results16, "|\n");
				Info += res.c_str();
			}
		}
		double** PsevdoA = Composition_Matrix_Raz(Inverse,TransA, M, N, N);
		FILE* results17;
		res = "";
		res = res + "\r\n" + "Псевдообратная матрица:" + "\r\n";
		Info += res.c_str();
		if (fopen_s(&results17, "ПсевдоА.txt", "w") == 0)
		{
			for (int i = 0; i < M; i++)
			{
				res = "";
				fprintf(results17, "| ");
				for (int j = 0; j < N; j++)
				{
					fprintf(results17, "%+*.5f ", 10, PsevdoA[i][j]);
					res = res + mylib::to_string(PsevdoA[i][j]) + "  ";
					if (j == N - 1)
						res += "\r\n";
				}
				fprintf(results17, "|\n");
				Info += res.c_str();
			}
		}
		double* stolbec = Stolbec(N);
		FILE* results18;
		res = "";
		res = res + "\r\n" + "Столбец свободных членов:" + "\r\n";
		if (fopen_s(&results18, "Столбец свободных членов.txt", "w") == 0)
		{
			for (int i = 0; i < N; i++)
			{
				fprintf(results18, "%+*.3f ", 10, stolbec[i]);
				res = res + mylib::to_string(stolbec[i]) + "  ";
			}
			Info += res.c_str();
		}

		double* x = Composition_Matrix_Stroka(PsevdoA, stolbec, N, M);
		FILE* results19;
		res = "";
		res = res + "\r\n" + "x:" + "\r\n";
		if (fopen_s(&results19, "x.txt", "w") == 0)
		{
			for (int i = 0; i < M; i++)
			{
				fprintf(results19, "%+*.3f ", 10, x[i]);
				res = res + mylib::to_string(x[i]) + "  ";
			}
			Info += res.c_str();
		}

		double* b = Composition_Matrix_Stroka(Inital, x, M, N);
		FILE* results20;
		res = "";
		res = res + "\r\n" + "b:" + "\r\n";
		if (fopen_s(&results20, "b.txt", "w") == 0)
		{
			for (int i = 0; i < N; i++)
			{
				fprintf(results20, "%+*.3f ", 10, b[i]);
				res = res + mylib::to_string(b[i]) + "  ";
			}
			Info += res.c_str();
		}
		double sum = 0;
		for (int i = 0; i < N; i++)
		{
			sum += (stolbec[i] - b[i]) * (stolbec[i] - b[i]);
		}
		nev = sum / M;
	}
	UpdateData(false);
}


void CMy3ZadachaITSingularRazlogenieDlg::OnBnClickedGetsignal()
{
	UpdateData(true);
	memset(SignalInput, 0, (Time + 1) * sizeof(double));
	MaxInitalData = 0;
	if(Gaus.GetCheck())
	{
		A1 = 2;
		A2 = 5;
		A3 = 3;
		G1 = 120;
		G2 = 100;
		G3 = 120;
		P = 50;
		//UpdateData(true);
		for (int i = 0; i < Time ; i++)
	{
		SignalInput[i] = InputSignal(i);
			if (SignalInput[i] > MaxInitalData)
				MaxInitalData = SignalInput[i];
	}
	}
	if (Garmon.GetCheck())
	{
		P = 70;
		for (int i = 0; i < Time; i++)
		{
			SignalInput[i] = PolySignal(i);
			if (SignalInput[i] > MaxInitalData)
				MaxInitalData = SignalInput[i];
		}
	}
	if (Zatux.GetCheck())
	{
		A1 = 0.001;
		A2 = 0.003;
		A3 = 0.001;
		P = 50;
		for (int i = 0; i < Time; i++)
		{
			SignalInput[i] = ZatuxSignal(i);
			if (SignalInput[i] > MaxInitalData)
				MaxInitalData = SignalInput[i];
		}
	}
	PererisovkaInitalData();
	PicDcInitalData->SelectObject(&graf_pen);
	PicDcInitalData->MoveTo(KOORD(0, SignalInput[0]));;
	for (int i = 0; i < Time; i++)
	{
		PicDcInitalData->LineTo(KOORD(i, SignalInput[i]));
	}
	UpdateData(false);

	//строим автокорреляционную матрицу
	double* rxx = new double[P];
	memset(rxx, 0, P* sizeof(double));
	double summ;
	for (int m = 0; m < P; m++)
	{
		summ = 0;
		for (int k = 0; k < P-m; k++)
		{
			summ += SignalInput[k] * SignalInput[k + m];
		}
		rxx[m] = summ / (P - m);
	}
	FILE* results10;
	if (fopen_s(&results10, "rxx.txt", "w") == 0)
	{
		for (int j = 0; j < P; j++)
		{
			fprintf(results10, "%+*.3f ", 10, rxx[j]);
		}
	}
	double** Rij = matrix(P, P);
	for (int i = 0; i < P; i++)
	{
		for (int j = 0; j < P; j++)
		{
			Rij[i][j] = 0;
	  }
	}
	int t = 0;
	for (int i = 0; i < P; i++)
	{
		for (int j = 0; j < P; j++)
		{
			if (i - j < 0)
			{
				t = P + (i - j);
				Rij[i][j] = rxx[t];
			}
			else
			Rij[i][j] = rxx[i-j];
		}
	}
	
	FILE* results7;
	if (fopen_s(&results7, "Rij.txt", "w") == 0)
	{
		for (int i = 0; i < P; i++)
		{
			fprintf(results7, "| ");
			for (int j = 0; j < P; j++)
			{
				fprintf(results7, "%+*.3f ", 10, Rij[i][j]);
			}
			fprintf(results7, "|\n");
		}
	}

	double* MassInStroka = new double[P * P];
	memset(MassInStroka, 0, (P * P ) * sizeof(double));
	int k = 0;
	for (int j = 0; j < P; j++)
	{
		for (int i = 0; i < P; i++)
		{
			MassInStroka[k] = Rij[i][j];
			k++;
		}
	}
	FILE* results9;
	if (fopen_s(&results9, "Массив в строку новый.txt", "w") == 0)
	{
		for (int j = 0; j < P*P; j++)
		{
			fprintf(results9, "%+*.3f ", 10, MassInStroka[j]);
		}
	}
	double* U = new double[P * P];
	double* V = new double[P * P];
	double* G = new double[P];

	memset(U, 0, P* P * sizeof(double));
	memset(V, 0, P* P * sizeof(double));
	memset(G, 0, P* sizeof(double));


	svd_hestenes(P, P, MassInStroka, U, V, G);

	FILE* results8;
	if (fopen_s(&results8, "Полученные массивы_new.txt", "w") == 0)
	{
		/*for (int j = 0; j < P * P; j++)
		{
			fprintf(results8, "%+*.3f ", 10, U[j]);
		}
		fprintf(results8, "\n");
		for (int i = 0; i < P * P; i++)
		{
			fprintf(results8, "%+*.3f ", 10, V[i]);
		}
		fprintf(results8, "\n");*/
		for (int k = 0; k < P; k++)
		{
			fprintf(results8, "%+*.3f ", 10, G[k]);
		}
		fprintf(results8, "\n");
	}
	double* GKrest = new double[P];
	memset(GKrest, 0, P* sizeof(double));
	for (int i = 0; i < P; i++)
	{
		if (G[i] != 0)
			GKrest[i] = 1 / G[i];
		else
			GKrest[i] = 0;
	}
	//псевдообратная матрица
	double** UKrest = matrix(P, P);
	double** VKrest = matrix(P, P);
	/*memset(UKrest, 0, P* P * sizeof(double));
	memset(VKrest, 0, P* P * sizeof(double));*/
	int s = 0;
	int x = 0;
	for (int i = 0; i < P; i++)
	{
		for (int j = 0; j < P; j++)
		{
			UKrest[i][j] = U[s];
			VKrest[i][j] = V[x];
			s++;
			x++;
		}
	}
	FILE* results37;
	if (fopen_s(&results37, "UKrest.txt", "w") == 0)
	{
		for (int i = 0; i < P; i++)
		{
			fprintf(results37, "| ");
			for (int j = 0; j < P; j++)
			{
				fprintf(results37, "%+*.3f ", 10, UKrest[i][j]);
			}
			fprintf(results37, "|\n");
		}
	}
	double** Akrest = matrix(P,P);
	////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////
	/////////////////////////////////////////////////////
	/////////////////////////////////////////////////////
	double** GGkrest = matrix(P,P);
	for (int i = 0; i < P; i++)
	{
		for(int j=0;j<P;j++)
		{
			if (i != j) GGkrest[i][j] = 0;
			else GGkrest[i][j] = G[i];
		}
	}
	double** Composition_Two = Composition_Matrix_Two(GGkrest,VKrest, P);
	Akrest = Composition_Matrix_Two(UKrest,Composition_Two, P);
	FILE* results12;
	if (fopen_s(&results12, "Должна быть псевдообратная матрица.txt", "w") == 0)
	{
		for (int i = 0; i < P; i++)
		{
			fprintf(results12, "| ");
			for (int j = 0; j < P; j++)
			{
				fprintf(results12, "%+*.3f ", 10, Akrest[i][j]);
			}
			fprintf(results12, "|\n");
		}
	}
	MaxGetData = 0;
	for (int i = 0; i < P; i++)
	{
		if (MaxGetData < G[i])
		{
			MaxGetData = G[i];
		}
	}
	PererisovkaGetData();
	PicDcGetData->SelectObject(&graf_pen2);
	PicDcGetData->MoveTo(KOORDGET(0, G[0]));;
	for (int i = 0; i < P; i++)
	{
		PicDcGetData->LineTo(KOORDGET(i, G[i]));
	}
	//UpdateData(false);
	VectorMax = 0;
	double* V1 = new double[(P + 1)];
	for (int i = 0; i <P; i++)
	{
		V1[i] = VKrest[i][number];
		
		//V1[i] = V[i * (P + 1) + 1];
		if (V1[i] > VectorMax)
		{
			VectorMax = V1[i]+0.1;
}
	}
	FILE* results21;
	if (fopen_s(&results21,"Собственный вектор.txt", "w") == 0)
	{
		
			fprintf(results21, "| ");
			for (int j = 0; j < P; j++)
			{
				fprintf(results21, "%+*.3f ", 10, V1[j]);
			}
			fprintf(results21, "|\n");
		
	}
	PererisovkaVector();
	PicDcSobstv->SelectObject(&graf_pen);
	PicDcSobstv->MoveTo(KOORDSOB(0, V1[0]));;
	for (int i = 0; i < P; i++)
	{
		PicDcSobstv->LineTo(KOORDSOB(i, V1[i]));
	}
}

