﻿
// 3ZadachaIT_SingularRazlogenieDlg.h: файл заголовка
//

#pragma once


// Диалоговое окно CMy3ZadachaITSingularRazlogenieDlg
class CMy3ZadachaITSingularRazlogenieDlg : public CDialogEx
{
// Создание
public:
	CMy3ZadachaITSingularRazlogenieDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MY3ZADACHAIT_SINGULARRAZLOGENIE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV
	CWnd* PicWndInitalData;
	CDC* PicDcInitalData;
	CRect PicInitalData;

	CWnd* PicWndGetData;
	CDC* PicDcGetData;
	CRect PicGetData;

	CWnd* PicWndSobstv;
	CDC* PicDcSobstv;
	CRect PicSobstv;


	CPen osi_pen;		// для осей 
	CPen setka_pen;		// для сетки
	CPen graf_pen;		// для графика функции
	CPen graf_pen2;
	CPen graf_pen3;

	//Переменные для работы с масштабом
	double xp, yp,			//коэфициенты пересчета
		xmin, xmax,			//максисимальное и минимальное значение х 
		ymin, ymax;			//максисимальное и минимальное значение y

	double xpget, ypget,			//коэфициенты пересчета
		xminget, xmaxget,			//максисимальное и минимальное значение х 
		yminget, ymaxget;			//максисимальное и минимальное значение y

	double xps, yps,			//коэфициенты пересчета
		xmins, xmaxs,			//максисимальное и минимальное значение х 
		ymins, ymaxs;			//максисимальное и минимальное значение y

// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSingrazlog();
	afx_msg void OnEnChangeN();
	// размерность матрицы
	int N;
	double determinant;
	int M;
	double A1;
	double A2;
	double A3;
	double t01;
	double t02;
	double t03;
	double G1;
	double G2;
	double G3;
	int Time;
	//порядок акм
	//int P = 50;
	double MaxInitalData = 0;
	double MaxGetData = 0;
	double VectorMax = 0;
	double normirovka = 0;
	double InputSignal(int t);
	double ZatuxSignal(double t);
	double* HardFunction = new double[Time + 1];
	double* SignalInput = new double[Time + 1];
	double* SignalOutput = new double[Time + 1];
	void PererisovkaInitalData();
	void PererisovkaGetData();
	void PererisovkaVector();
	afx_msg void OnBnClickedGetsignal();
	CButton Garmon;
	CButton Gaus;
	CButton Zatux;
	double PolySignal(double t);
	double F1;
	double F2;
	double F3;
	double P1;
	double P2;
	double P3;
	//порядок акм
	int P;
	double nev;
	CString Info;
	afx_msg void True();
	int number;
};
