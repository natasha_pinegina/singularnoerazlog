﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется My3ZadachaITSingularRazlogenie.rc
//
#define IDD_MY3ZADACHAIT_SINGULARRAZLOGENIE_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_SingRazlog                  1000
#define IDC_GetSignal                   1001
#define IDC_N                           1002
#define IDC_Determinant                 1003
#define IDC_N2                          1004
#define IDC_M                           1005
#define IDC_InputSignal                 1006
#define IDC_A1                          1007
#define IDC_A2                          1008
#define IDC_A3                          1009
#define IDC_t01                         1010
#define IDC_t02                         1011
#define IDC_t03                         1012
#define IDC_G1                          1013
#define IDC_G2                          1014
#define IDC_G3                          1015
#define IDC_M2                          1016
#define IDC_M3                          1017
#define IDC_M4                          1018
#define IDC_Time                        1019
#define IDC_Garmon                      1020
#define IDC_Gaus                        1021
#define IDC_Zatux                       1022
#define IDC_F1                          1023
#define IDC_F2                          1024
#define IDC_F3                          1025
#define IDC_M5                          1026
#define IDC_P1                          1027
#define IDC_P2                          1028
#define IDC_P3                          1029
#define IDC_M6                          1030
#define IDC_SobctChisla                 1031
#define IDC_P                           1032
#define IDC_Nev                         1033
#define IDC_COMBO1                      1034
#define IDC_P4                          1034
#define IDC_EDIT1                       1035
#define IDC_Info                        1035
#define IDC_M7                          1037
#define IDC_SobctvVect                  1038

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1036
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
